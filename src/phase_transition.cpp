
/*@ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 **
 **    Copyright (c) 2011-2020, JGU Mainz, Anton Popov, Boris Kaus
 **    All rights reserved.
 **
 **    This software was developed at:
 **
 **         Institute of Geosciences
 **         Johannes-Gutenberg University, Mainz
 **         Johann-Joachim-Becherweg 21
 **         55128 Mainz, Germany
 **
 **    project:    LaMEM
 **    filename:   AVD.c
 **
 **    LaMEM is free software: you can redistribute it and/or modify
 **    it under the terms of the GNU General Public License as published
 **    by the Free Software Foundation, version 3 of the License.
 **
 **    LaMEM is distributed in the hope that it will be useful,
 **    but WITHOUT ANY WARRANTY; without even the implied warranty of
 **    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 **    See the GNU General Public License for more details.
 **
 **    You should have received a copy of the GNU General Public License
 **    along with LaMEM. If not, see <http://www.gnu.org/licenses/>.
 **
 **
 **    Contact:
 **        Boris Kaus       [kaus@uni-mainz.de]
 **        Anton Popov      [popov@uni-mainz.de]
 **
 **
 **    Main development team:
 **         Anton Popov      [popov@uni-mainz.de]
 **         Boris Kaus       [kaus@uni-mainz.de]
 **			Andrea Piccolo 
 ** 		Jianfeng Yang
 **         Tobias Baumann
 **         Adina Pusok
 **		
 **		Main responsible persons for this routine:
 **			Andrea Piccolo
 **			Jianfeng Yang
 **			Boris Kaus
 **
 ** ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ @*/
/*	Bibliography reference for the phase transition
 * All the phase transition listed are coming from [1] (Tab.1).
 * [1] Manuele Faccenda, Luca Dal Zilio, The role of solid–solid phase transitions in mantle convection, Lithos,
        Volumes 268–271, 2017,
 * [2]B.R. Hacker, G.A. Abers, S.M. Peacock Subduction factory 1. Theoretical mineralogy, densities, seismic wave speeds, and H2O contents
        Journal of Geophysical Research, 108 (2003), 10.1029/2001JB001127
 * [3] Hydrous solidus of CMAS-pyrolite and melting of mantle plumes at the bottom of the upper mantle
		Geophysical Research Letters, 30 (2003), 10.1029/2003GL018318
 * [4] E.R. Hernandez, J. Brodholt, D. Alfè Structural, vibrational and thermodynamic properties of Mg2SiO4 and MgSiO3 minerals from first-principles simulations
	   Physics of the Earth and Planetary Interiors, 240 (2015), pp. 1-24
 * [5] The postspinel boundary in pyrolitic compositions determined in the laser-heated diamond anvil cell
	    Geophysical Research Letters, 41 (2014), pp. 3833-3841
 */
/*
	The routines in this file allow changing the phase of a marker depending on conditions
	set by the user.
	They thus allow adding phase transitions to a setup in a rahther simple manner.
	Moreover, a number of phase transitions have been predefined as profiles, such as
	the basalt-eclogite reaction.
*/

#include "LaMEM.h"
#include "AVD.h"
#include "advect.h"
#include "scaling.h"
#include "JacRes.h"
#include "fdstag.h"
#include "bc.h"
#include "tools.h"
#include "phase_transition.h"
#include "phase.h"
#include "constEq.h"
#include "parsing.h"
#include "objFunct.h"
//-----------------------------------------------------------------//
#undef __FUNCT__
#define __FUNCT__ "DBMatReadPhaseTr"
PetscErrorCode DBMatReadPhaseTr(DBMat *dbm, FB *fb)
{
	// read phase transitions from file
	PetscFunctionBegin;

	Ph_trans_t      *ph;
	PetscInt        ID, i;
	PetscErrorCode  ierr;
    char            str_direction[_str_len_],Type_[_str_len_];

	// Phase transition law ID
	ierr    =   getIntParam(fb, _REQUIRED_, "ID", &ID, 1, dbm->numPhtr-1); CHKERRQ(ierr);

	// get pointer to specified softening law
	ph      =   dbm->matPhtr + ID;

	// check ID
	if(ph->ID != -1)
	{
		 SETERRQ(PETSC_COMM_WORLD, PETSC_ERR_USER, "Duplicate phase transition law!");
	}

	// set ID
	ph->ID  =   ID;
	ierr    =   getStringParam(fb, _REQUIRED_, "Type",Type_,NULL);  CHKERRQ(ierr);

	if(!strcmp(Type_,"Constant"))
	{
		ph->Type = _Constant_;
		ierr    =   Set_Constant_Phase_Transition(ph, dbm, fb,ID);    CHKERRQ(ierr);
	}
	else if(!strcmp(Type_,"Clapeyron"))
	{
		ph->Type = _Clapeyron_;
		ierr    =   Set_Clapeyron_Phase_Transition(ph, dbm, fb,ID);   CHKERRQ(ierr);
	}



	ierr = getIntParam(fb,      _OPTIONAL_, "number_phases", &ph->number_phases,1 ,                     _max_num_tr_);      CHKERRQ(ierr);
	ierr = getIntParam(fb,      _OPTIONAL_, "PhaseBelow",       ph->PhaseBelow,     ph->number_phases , _max_num_phases_);  CHKERRQ(ierr);
	ierr = getIntParam(fb, 	    _OPTIONAL_, "PhaseAbove",       ph->PhaseAbove,     ph->number_phases , _max_num_phases_);  CHKERRQ(ierr);
	ierr = getScalarParam(fb,   _OPTIONAL_, "DensityBelow",     ph->DensityBelow,   ph->number_phases , 1.0);               CHKERRQ(ierr);
	ierr = getScalarParam(fb,   _OPTIONAL_, "DensityAbove",     ph->DensityAbove,   ph->number_phases,  1.0);               CHKERRQ(ierr);

    ierr = getStringParam(fb, _OPTIONAL_, "PhaseDirection", 	str_direction, "BothWays"); 					            CHKERRQ(ierr);  
	if     	(!strcmp(str_direction, "BelowToAbove"))    ph->PhaseDirection  = 1;
	else if (!strcmp(str_direction, "AboveToBelow"))    ph->PhaseDirection  = 2;
    else if (!strcmp(str_direction, "BothWays"    ))    ph->PhaseDirection  = 0;
    else{      SETERRQ1(PETSC_COMM_WORLD, PETSC_ERR_USER, "Unknown Phase direction %s \n", str_direction);  }
        

	if (!ph->PhaseAbove || !ph->PhaseBelow)
	{
		SETERRQ1(PETSC_COMM_WORLD, PETSC_ERR_USER, "You have not specify the correct phase transition type (Constant) (Clapeyron) ", (LLD)ID);
	}
    
    PetscPrintf(PETSC_COMM_WORLD,"     Phase Above        :  ");
    for (i=0; i<ph->number_phases; i++){    PetscPrintf(PETSC_COMM_WORLD," %d ", (LLD)(ph->PhaseAbove[i])); }
    PetscPrintf(PETSC_COMM_WORLD," \n");

    PetscPrintf(PETSC_COMM_WORLD,"     Phase Below        :  ");
    for (i=0; i<ph->number_phases; i++){    PetscPrintf(PETSC_COMM_WORLD," %d ", (LLD)(ph->PhaseBelow[i])); }
    PetscPrintf(PETSC_COMM_WORLD," \n");
    PetscPrintf(PETSC_COMM_WORLD,"     Direction          :   %s \n", str_direction);
    
	PetscFunctionReturn(0);
}
//----------------------------------------------------------------------------------------------------------//
#undef __FUNCT__
#define __FUNCT__ "Set_Constant_Phase_Transition"
PetscErrorCode  Set_Constant_Phase_Transition(Ph_trans_t   *ph, DBMat *dbm, FB *fb,PetscInt ID)
{
	Scaling      *scal;
	char         Parameter[_str_len_];
	PetscErrorCode ierr;
	PetscFunctionBegin;

	scal = dbm -> scal;

	ierr = getStringParam(fb, _REQUIRED_, "Parameter_transition",   Parameter, "none");  CHKERRQ(ierr);
	if(!strcmp(Parameter, "T"))
	{
		ph->Parameter_transition = _T_;
	}
	else if(!strcmp(Parameter, "P"))
	{
		ph->Parameter_transition = _Pressure_;
	}
	else if(!strcmp(Parameter, "Depth"))
	{
		ph->Parameter_transition = _Depth_;
	}
	else if(!strcmp(Parameter, "APS"))
	{
		ph->Parameter_transition = _PlasticStrain_;
	}

	ierr = getScalarParam(fb, _REQUIRED_, "ConstantValue",          &ph->ConstantValue,        1,1.0);  CHKERRQ(ierr);


	PetscPrintf(PETSC_COMM_WORLD,"   Phase Transition [%lld] :   Constant \n", (LLD)(ph->ID));
    PetscPrintf(PETSC_COMM_WORLD,"     Parameter          :   %s \n",    Parameter);
    PetscPrintf(PETSC_COMM_WORLD,"     Transition Value   :   %1.3f \n", ph->ConstantValue);

	if(ph->Parameter_transition==_T_)                   //  Temperature [Celcius]
	{
		ph->ConstantValue   =   (ph->ConstantValue + scal->Tshift)/scal->temperature;
	}
	else if(ph->Parameter_transition==_Pressure_)       //  Pressure [Pa]
	{
		ph->ConstantValue   /= scal->stress_si;
	}
	else if(ph->Parameter_transition==_Depth_)          //  Depth [km if geo units]
	{
		ph->ConstantValue   /= scal->length;
	}
	else if(ph->Parameter_transition==_PlasticStrain_)  //  accumulated plastic strain
	{
		ph->ConstantValue   = ph->ConstantValue;        // is already in nd units
	}
    else{
        SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_USER, "Unknown parameter for [Constant] Phase transition");
    }


	PetscFunctionReturn(0);

}
//------------------------------------------------------------------------------------------------------------//
#undef __FUNCT__
#define __FUNCT__ "Set_Constant_Phase_Transition"
PetscErrorCode  Set_Clapeyron_Phase_Transition(Ph_trans_t   *ph, DBMat *dbm, FB *fb, PetscInt ID)
{
	PetscFunctionBegin;

	PetscErrorCode  ierr;
	Scaling         *scal;
	PetscInt        it=0;

	scal    = dbm -> scal;
	ierr    = getStringParam(fb, _OPTIONAL_, "Name_Clapeyron", ph->Name_clapeyron, "none");  CHKERRQ(ierr);
	if (ph->Name_clapeyron)
	{
		ierr = SetClapeyron_Eq(ph); CHKERRQ(ierr);
		PetscPrintf(PETSC_COMM_WORLD,"   Phase Transition [%lld] :   Clapeyron \n", (LLD)(ph->ID));
        PetscPrintf(PETSC_COMM_WORLD,"     Transition law     :   %s\n", ph->Name_clapeyron);
    }

	ierr = getIntParam   (fb, _OPTIONAL_, "numberofequation",   &ph->neq,           1,          2.0); CHKERRQ(ierr);
	if(ph->neq>2 || ph->neq == 0)
	{
		SETERRQ1(PETSC_COMM_WORLD, PETSC_ERR_USER, "If you are using any Clapeyron phase transition you cannot have a number of equation higher than 2, or equal to zero", (LLD)ID);

	}

	ierr = getScalarParam(fb, _OPTIONAL_, "clapeyron_slope",    ph->clapeyron_slope,ph->neq,    1.0); CHKERRQ(ierr);    // units??
	ierr = getScalarParam(fb, _OPTIONAL_, "P0_clapeyron",       ph->P0_clapeyron,   ph->neq,    1.0); CHKERRQ(ierr);    // units??
	ierr = getScalarParam(fb, _OPTIONAL_, "T0_clapeyron",       ph->T0_clapeyron,   ph->neq,    1.0); CHKERRQ(ierr);    // units??        

	if((!ph->clapeyron_slope || !ph->T0_clapeyron || !ph->clapeyron_slope   ||  !ph->Name_clapeyron))
	{
		SETERRQ1(PETSC_COMM_WORLD, PETSC_ERR_USER, "If you are using any Clapeyron phase transition avaiable you need to specify P0, T0, gamma and the number of equations ( P=(T-T0)*gamma +(P0) ).", (LLD)ID);
	}

    PetscPrintf(PETSC_COMM_WORLD,"       # Equations      :   %i    [ P = P0 + gamma*(T-T0) ] \n", ph->neq);
        
	for(it=0; it<ph->neq; it++)
	{
        PetscPrintf(PETSC_COMM_WORLD,"       eq[%i]            :   gamma = %- 4.2e [MPa/C], P0 = %4.2e [Pa],  T0 = %2.1f [deg C] \n", it, ph->clapeyron_slope[it], ph->P0_clapeyron[it],ph->T0_clapeyron[it]);

		ph->clapeyron_slope[it]     *=  1e6*(scal->temperature/scal->stress_si);                    // [K/MPa]
		ph->P0_clapeyron[it]        /=  (scal->stress_si);                                          // [Pa]
		ph->T0_clapeyron[it]        =   (ph->T0_clapeyron[it]+scal->Tshift)/ (scal->temperature);   // [Celcius]
	}
	PetscFunctionReturn(0);

}

// ---------------------------------------------------------------------------------------------------------- //
#undef __FUNCT__
#define __FUNCT__ "Overwrite_Density"
PetscErrorCode  Overwrite_density(DBMat *dbm)
{
    /* 
        This changes the density values of the phases involved with a pre-defined phase transition such 
        that they are compatible (e.g., tthis ensures that we do not use bogus values for the basalt->eclogite transition,
        for example) 
    */

	PetscFunctionBegin;
	Scaling         *scal;
	Ph_trans_t      *ph;
	Material_t      *mat;
	PetscInt        numPhTrn,   nPtr,       num_phases, iter, jj1, jj2;
	PetscScalar     rho_above,  rho_below,  rho_scal;
    
    // Retrieve parameters:
    scal        = dbm->scal;
	rho_scal    = scal->density;
	mat         = dbm->phases;
	numPhTrn    = dbm->numPhtr;

    PetscPrintf(PETSC_COMM_WORLD,"\n   Adjusting density values due to phase transitions: \n");
    for(nPtr=0; nPtr<numPhTrn;  nPtr++)
	{
		ph          = dbm->matPhtr+nPtr;
		num_phases  = ph->number_phases;

		for(iter=0; iter<num_phases;    iter++)
		{
			rho_above = ph->DensityAbove[iter];
			rho_below = ph->DensityBelow[iter];
			if(rho_above > 0 && rho_below >0)
			{
				jj1             =   ph->PhaseBelow[iter];
				mat[jj1].rho    =   rho_below/rho_scal;
				PetscPrintf(PETSC_COMM_WORLD,"     Phase              : %4d, rho = %4.1f %s \n",jj1,rho_below, scal->lbl_density);

				jj2             =   ph->PhaseAbove[iter];
				mat[jj2].rho    =   rho_above/rho_scal;
				PetscPrintf(PETSC_COMM_WORLD,"     Phase              : %4d, rho = %4.1f %s \n",jj2,rho_above, scal->lbl_density);
			}

		}
    }
	PetscFunctionReturn(0);
}

//-----------------------------------------------------------------------------------------------------------//
#undef __FUNCT__
#define __FUNCT__ "SetClapeyron_Eq"
PetscErrorCode SetClapeyron_Eq(Ph_trans_t *ph)
{
	/*
        This has predefined phase transitions that are specified as linear lines, where
        the phase transition is given by
        
        P = P0 + clapeyron_slope*(T-T0)

        Where 
            P0              :   Zero interection [Pa]
            clapeyron_slope :   Slope of the line [MPa/C]??
            T0              :   Shift in temperature [Celcius]
    */

	PetscFunctionBegin;
	if (!strcmp(ph->Name_clapeyron,"Eclogite"))
	{
		//[1][2]
		ph->neq                 =   2;
		ph->P0_clapeyron[0]     =   2e9;        // Pa
		ph->T0_clapeyron[0]     =   800;        // C
		ph->clapeyron_slope[0]  =   1.5;
		
        ph->P0_clapeyron[1]     =   2e9;        // Pa    
		ph->T0_clapeyron[1]     =   700;
		ph->clapeyron_slope[1]  =   -30;
	}
	else if(!strcmp(ph->Name_clapeyron,"Mantle_Transition_WadsleyiteRingwoodite_wet"))
	{
		// [1][3]
		ph->neq                 =   1;
		ph->P0_clapeyron[0]     =   13.5e9;
		ph->T0_clapeyron[0]     =   1537;
		ph->clapeyron_slope[0]  =   5;
	}
	else if(!strcmp(ph->Name_clapeyron,"Mantle_Transition_WadsleyiteRingwoodite_dry"))
	{
		//[1][4]
		ph->neq                 =   1;
		ph->P0_clapeyron[0]     =   18e9;
		ph->T0_clapeyron[0]     =   1597;
		ph->clapeyron_slope[0]  =   3.5;
	}
	else if(!strcmp(ph->Name_clapeyron,"Mantle_Transition_660km"))
	{
		//[1][5]
		ph->neq                 =   1;
		ph->P0_clapeyron[0]     =   23e9;
		ph->T0_clapeyron[0]     =   1667;
		ph->clapeyron_slope[0]  =   -2.5;
	}

	PetscFunctionReturn(0);
}
//===========================================================================================================//
#undef __FUNCT__
#define __FUNCT__ "Phase_Transition"
PetscErrorCode Phase_Transition(AdvCtx *actx)
{
	// creates arrays to optimize marker-cell interaction
	PetscFunctionBegin;

    PetscErrorCode  ierr;
    DBMat           *dbm;
	Ph_trans_t      *PhaseTrans;
	Marker          *P;
	JacRes          *jr;
	PetscInt        i, ph,nPtr, numPhTrn,below,above,num_phas;
	PetscInt        PH1,PH2;
    PetscLogDouble  t;


		
    // Retrieve parameters
	jr          =   actx->jr;
	dbm         =   jr->dbm;
	numPhTrn    =   dbm->numPhtr;

	if (!numPhTrn) 	PetscFunctionReturn(0);		// only execute this function if we have phase transitions

    PrintStart(&t, "Phase_Transition", NULL);

	// loop over all local particles 		PetscPrintf(PETSC_COMM_WORLD,"PHASE = %d  i = %d, counter = %d\n",P->phase,i,counter);
	nPtr        =   0;
	for(nPtr=0; nPtr<numPhTrn; nPtr++)
	{
		PhaseTrans = jr->dbm->matPhtr+nPtr;
		for(i = 0; i < actx->nummark; i++)      // loop over all (local) particles
		{

			P   =   &actx->markers[i];      // retrieve marker


			num_phas    =   PhaseTrans->number_phases;
			below       =   Check_Phase_above_below(PhaseTrans->PhaseBelow, P, num_phas);
			above       =   Check_Phase_above_below(PhaseTrans->PhaseAbove, P, num_phas);

			if  ( (below >= 0) || (above >= 0) )
			{
				PH2 = P->phase;
				PH1 = P->phase;
                 // the current phase is indeed involved in a phase transition
				if      (   below>=0    )
				{
					PH1 = PhaseTrans->PhaseBelow[below];
					PH2 = PhaseTrans->PhaseAbove[below];
				}
				else if (   above >=0   )
				{
					PH1 = PhaseTrans->PhaseBelow[above];
					PH2 = PhaseTrans->PhaseAbove[above];
				}
			
				ph          =   Transition(PhaseTrans, P, PH1, PH2);

                if (PhaseTrans->PhaseDirection==0){
                     P->phase    =   ph;
                }
                else if ( (PhaseTrans->PhaseDirection==1) & (below>=0) ){
                    P->phase    =   ph;
                }
                else if ( (PhaseTrans->PhaseDirection==2) & (above>=0) ){
                    P->phase    =   ph;
                }


			}
		}

	}
	ierr = ADVInterpMarkToCell(actx);   CHKERRQ(ierr);

    PrintDone(t);

	PetscFunctionReturn(0);
}

//----------------------------------------------------------------------------------------
PetscInt Transition(Ph_trans_t *PhaseTrans, Marker *P, PetscInt PH1,PetscInt PH2)
{
	PetscInt ph;

	ph = P->phase;
	if(PhaseTrans->Type==_Constant_)    // NOTE: string comparisons can be slow; we can change this to integers if needed
	{
		ph = Check_Constant_Phase_Transition(PhaseTrans,P,PH1,PH2);
	}
	else if(PhaseTrans->Type==_Clapeyron_)
	{
		ph = Check_Clapeyron_Phase_Transition(PhaseTrans,P,PH1,PH2);
	}


	return ph;
}

/*------------------------------------------------------------------------------------------------------------
    Sets the values for a phase transition that occurs @ a constant value
*/
PetscInt Check_Constant_Phase_Transition(Ph_trans_t *PhaseTrans,Marker *P,PetscInt PH1, PetscInt PH2) 
{
    
    PetscInt ph;

	ph = 0;
	if((PhaseTrans->Parameter_transition==_T_))   // NOTE: string comparisons can be slow; optimization possibility
		{
            // Temperature transition
            if ( P->T >= PhaseTrans->ConstantValue)     {   ph = PH2; }
			else                                        {   ph = PH1; }
		}

	if(PhaseTrans->Parameter_transition==_Pressure_)
		{
            if  ( P->p >= PhaseTrans->ConstantValue)    {   ph = PH2;   }
		    else                                        {   ph = PH1;   }
          
		}

	if(PhaseTrans->Parameter_transition==_Depth_)
		{
          if ( P->X[2] >= PhaseTrans->ConstantValue)  {   ph = PH2;   }
          else                                        {   ph = PH1;   }
        }

	if(PhaseTrans->Parameter_transition==_PlasticStrain_) // accumulated plastic strain
		{
            if ( P->APS >= PhaseTrans->ConstantValue)  {   ph = PH2;        }
            else                                       {   ph = PH1;        }
        }

	return ph;
}

//------------------------------------------------------------------------------------------------------------//
PetscInt Check_Clapeyron_Phase_Transition(Ph_trans_t *PhaseTrans,Marker *P,PetscInt PH1, PetscInt PH2)
{
	PetscInt ph,ip,neq;
	PetscScalar Pres[2];


	neq = PhaseTrans->neq;
	for (ip=0; ip<neq; ip++)
	{
		Pres[ip]    =   (P->T - PhaseTrans->T0_clapeyron[ip]) * PhaseTrans->clapeyron_slope[ip] + PhaseTrans->P0_clapeyron[ip];
	}
	if (neq==1)
	{
        if  ( P->p >= Pres[0]) {   ph  =   PH2;    }
        else                   {   ph  =   PH1;    }
	}
	else
	{
        // in case we have two equations to describe the phase transition:
        if  ( (P->p >= Pres[0]) && (P->p >= Pres[1]) )      {   ph  =   PH2;    }
        else                                                {   ph  =   PH1;    }

	}

	return ph;
}

//------------------------------------------------------------------------------------------------------------//
PetscInt Check_Phase_above_below(PetscInt *phase_array, Marker *P,PetscInt num_phas)
{
	PetscInt n,it,size;
	size = num_phas;
	it=0;
	for(it=0;it<size;it++)
	{
		n=-1;
		if(P->phase==phase_array[it])
		{
			n=it;
			break;
		}
	}

	return n;
}
